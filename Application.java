public class Application
{
	public static void main(String[] args)
	{
		Rat rat1 = new Rat();
		rat1.ageInDays = 7;
		rat1.colorAndPattern = Rat.Colors.BROWN;
		rat1.isDomestic = false;
		
		// System.out.println(rat1.ageInDays);
		// System.out.println(rat1.colorAndPattern);
		// System.out.println(rat1.isDomestic);
		
		rat1.canLearnTricks(rat1.isDomestic);
		rat1.canGroomItself(rat1.ageInDays);
		
		Rat rat2 = new Rat();
		rat2.ageInDays = 125;
		rat2.colorAndPattern = Rat.Colors.SPOTTED;
		rat2.isDomestic = true;
		
		// System.out.println(rat2.ageInDays);
		// System.out.println(rat2.colorAndPattern);
		// System.out.println(rat2.isDomestic);
		
		rat2.canLearnTricks(rat2.isDomestic);
		rat2.canGroomItself(rat2.ageInDays);
		
		Rat[] mischief = new Rat[3];
		mischief[0] = rat1;
		mischief[1] = rat2;
		
		System.out.println(mischief[1].colorAndPattern);
		
		mischief[2] = new Rat();
		
		mischief[2].ageInDays = 365;
		mischief[2].colorAndPattern = Rat.Colors.WHITE;
		mischief[2].isDomestic = true;
		
		System.out.println(mischief[2].ageInDays);
		System.out.println(mischief[2].colorAndPattern);
		System.out.println(mischief[2].isDomestic);
	}
}