public class Rat
{
	public int ageInDays;
	public enum Colors {WHITE, BLACK, GRAY, SPOTTED, BROWN;}
	Colors colorAndPattern;
	public boolean isDomestic;
	
	public void canLearnTricks(boolean isDomestic)
	{
		if(isDomestic)
		{
			System.out.println("This rat knows how to sit, roll over and jump on command!");
		}
		else
		{
			System.out.println("This rat is not domesticated, it cannot learn tricks");
		}
	}
	
	public void canGroomItself(int ageInDays)
	{
		if(ageInDays>=35)
		{
			System.out.println("This rat can groom itself!");
		}
		else
		{
			System.out.println("This rat is too young to groom itself.");
		}
	}
}